﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Despesas_API_PW2.Models
{
    public class Rendimento
    {
        public int RendimentoId { get; set; }
        public int Valor { get; set; }
        public int TipoRendimentoId { get; set; }
        public virtual TipoRendimento TipoRendimento { get; set; }
        [DataType(DataType.Date)]
        public DateTime DataPagamento { get; set; }
        public string UserId { get; set; }
    }
}
