﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Despesas_API_PW2.Models
{
    public class Despesa
    {
        public int DespesaID { get; set; }
        public int Valor { get; set; }
        public int TipoDespesaId { get; set; }
        public virtual TipoDespesa TipoDespesa { get; set; }
        public int MeioPagamentoId { get; set; }
        public virtual MeioPagamento MeioPagamento { get; set; }
        [DataType(DataType.Date)]
        public DateTime DataPagamento { get; set; }
        public string UserId { get; set; }
    }
}
