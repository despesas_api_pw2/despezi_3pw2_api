﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Despesas_API_PW2.Models
{
    public class MeioPagamento
    {
        public int MeioPagamentoId { get; set; }
        public string Nome { get; set; }
        public string UserId { get; set; }
    }
}
