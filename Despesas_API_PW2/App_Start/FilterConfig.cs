﻿using System.Web;
using System.Web.Mvc;

namespace Despesas_API_PW2
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
