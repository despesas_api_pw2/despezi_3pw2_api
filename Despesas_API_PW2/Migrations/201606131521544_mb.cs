namespace Despesas_API_PW2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Despesas",
                c => new
                    {
                        DespesaID = c.Int(nullable: false, identity: true),
                        Valor = c.Int(nullable: false),
                        TipoDespesaId = c.Int(nullable: false),
                        MeioPagamentoId = c.Int(nullable: false),
                        DataPagamento = c.DateTime(nullable: false),
                        UserId = c.String(),
                    })
                .PrimaryKey(t => t.DespesaID)
                .ForeignKey("dbo.MeioPagamentoes", t => t.MeioPagamentoId, cascadeDelete: true)
                .ForeignKey("dbo.TipoDespesas", t => t.TipoDespesaId, cascadeDelete: true)
                .Index(t => t.TipoDespesaId)
                .Index(t => t.MeioPagamentoId);
            
            CreateTable(
                "dbo.MeioPagamentoes",
                c => new
                    {
                        MeioPagamentoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        UserId = c.String(),
                    })
                .PrimaryKey(t => t.MeioPagamentoId);
            
            CreateTable(
                "dbo.TipoDespesas",
                c => new
                    {
                        TipoDespesaId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        UserId = c.String(),
                    })
                .PrimaryKey(t => t.TipoDespesaId);
            
            CreateTable(
                "dbo.Rendimentoes",
                c => new
                    {
                        RendimentoId = c.Int(nullable: false, identity: true),
                        Valor = c.Int(nullable: false),
                        TipoRendimentoId = c.Int(nullable: false),
                        DataPagamento = c.DateTime(nullable: false),
                        UserId = c.String(),
                    })
                .PrimaryKey(t => t.RendimentoId)
                .ForeignKey("dbo.TipoRendimentoes", t => t.TipoRendimentoId, cascadeDelete: true)
                .Index(t => t.TipoRendimentoId);
            
            CreateTable(
                "dbo.TipoRendimentoes",
                c => new
                    {
                        TipoRendimentoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        UserId = c.String(),
                    })
                .PrimaryKey(t => t.TipoRendimentoId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Rendimentoes", "TipoRendimentoId", "dbo.TipoRendimentoes");
            DropForeignKey("dbo.Despesas", "TipoDespesaId", "dbo.TipoDespesas");
            DropForeignKey("dbo.Despesas", "MeioPagamentoId", "dbo.MeioPagamentoes");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Rendimentoes", new[] { "TipoRendimentoId" });
            DropIndex("dbo.Despesas", new[] { "MeioPagamentoId" });
            DropIndex("dbo.Despesas", new[] { "TipoDespesaId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.TipoRendimentoes");
            DropTable("dbo.Rendimentoes");
            DropTable("dbo.TipoDespesas");
            DropTable("dbo.MeioPagamentoes");
            DropTable("dbo.Despesas");
        }
    }
}
