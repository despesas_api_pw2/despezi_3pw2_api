﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Despesas_API_PW2.Models;

namespace Despesas_API_PW2.Controllers
{
    public class TipoRendimentoesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/TipoRendimentoes
        public IQueryable<TipoRendimento> GetTipoRendimentoes()
        {
            return db.TipoRendimentoes;
        }

        // GET: api/TipoRendimentoes/5
        [ResponseType(typeof(TipoRendimento))]
        public async Task<IHttpActionResult> GetTipoRendimento(int id)
        {
            TipoRendimento tipoRendimento = await db.TipoRendimentoes.FindAsync(id);
            if (tipoRendimento == null)
            {
                return NotFound();
            }

            return Ok(tipoRendimento);
        }

        // PUT: api/TipoRendimentoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTipoRendimento(int id, TipoRendimento tipoRendimento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoRendimento.TipoRendimentoId)
            {
                return BadRequest();
            }

            db.Entry(tipoRendimento).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoRendimentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoRendimentoes
        [ResponseType(typeof(TipoRendimento))]
        public async Task<IHttpActionResult> PostTipoRendimento(TipoRendimento tipoRendimento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TipoRendimentoes.Add(tipoRendimento);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tipoRendimento.TipoRendimentoId }, tipoRendimento);
        }

        // DELETE: api/TipoRendimentoes/5
        [ResponseType(typeof(TipoRendimento))]
        public async Task<IHttpActionResult> DeleteTipoRendimento(int id)
        {
            TipoRendimento tipoRendimento = await db.TipoRendimentoes.FindAsync(id);
            if (tipoRendimento == null)
            {
                return NotFound();
            }

            db.TipoRendimentoes.Remove(tipoRendimento);
            await db.SaveChangesAsync();

            return Ok(tipoRendimento);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipoRendimentoExists(int id)
        {
            return db.TipoRendimentoes.Count(e => e.TipoRendimentoId == id) > 0;
        }
    }
}