﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Despesas_API_PW2.Models;

namespace Despesas_API_PW2.Controllers
{
    public class MeioPagamentoesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/MeioPagamentoes
        public IQueryable<MeioPagamento> GetMeioPagamentoes()
        {
            return db.MeioPagamentoes;
        }

        // GET: api/MeioPagamentoes/5
        [ResponseType(typeof(MeioPagamento))]
        public async Task<IHttpActionResult> GetMeioPagamento(int id)
        {
            MeioPagamento meioPagamento = await db.MeioPagamentoes.FindAsync(id);
            if (meioPagamento == null)
            {
                return NotFound();
            }

            return Ok(meioPagamento);
        }

        // PUT: api/MeioPagamentoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMeioPagamento(int id, MeioPagamento meioPagamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != meioPagamento.MeioPagamentoId)
            {
                return BadRequest();
            }

            db.Entry(meioPagamento).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MeioPagamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MeioPagamentoes
        [ResponseType(typeof(MeioPagamento))]
        public async Task<IHttpActionResult> PostMeioPagamento(MeioPagamento meioPagamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MeioPagamentoes.Add(meioPagamento);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = meioPagamento.MeioPagamentoId }, meioPagamento);
        }

        // DELETE: api/MeioPagamentoes/5
        [ResponseType(typeof(MeioPagamento))]
        public async Task<IHttpActionResult> DeleteMeioPagamento(int id)
        {
            MeioPagamento meioPagamento = await db.MeioPagamentoes.FindAsync(id);
            if (meioPagamento == null)
            {
                return NotFound();
            }

            db.MeioPagamentoes.Remove(meioPagamento);
            await db.SaveChangesAsync();

            return Ok(meioPagamento);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MeioPagamentoExists(int id)
        {
            return db.MeioPagamentoes.Count(e => e.MeioPagamentoId == id) > 0;
        }
    }
}