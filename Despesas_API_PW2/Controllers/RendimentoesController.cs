﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Despesas_API_PW2.Models;

namespace Despesas_API_PW2.Controllers
{
    public class RendimentoesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Rendimentoes
        public IQueryable<Rendimento> GetRendimentoes()
        {
            return db.Rendimentoes;
        }

        // GET: api/Rendimentoes/5
        [ResponseType(typeof(Rendimento))]
        public async Task<IHttpActionResult> GetRendimento(int id)
        {
            Rendimento rendimento = await db.Rendimentoes.FindAsync(id);
            if (rendimento == null)
            {
                return NotFound();
            }

            return Ok(rendimento);
        }

        // PUT: api/Rendimentoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRendimento(int id, Rendimento rendimento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rendimento.RendimentoId)
            {
                return BadRequest();
            }

            db.Entry(rendimento).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RendimentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Rendimentoes
        [ResponseType(typeof(Rendimento))]
        public async Task<IHttpActionResult> PostRendimento(Rendimento rendimento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Rendimentoes.Add(rendimento);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = rendimento.RendimentoId }, rendimento);
        }

        // DELETE: api/Rendimentoes/5
        [ResponseType(typeof(Rendimento))]
        public async Task<IHttpActionResult> DeleteRendimento(int id)
        {
            Rendimento rendimento = await db.Rendimentoes.FindAsync(id);
            if (rendimento == null)
            {
                return NotFound();
            }

            db.Rendimentoes.Remove(rendimento);
            await db.SaveChangesAsync();

            return Ok(rendimento);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RendimentoExists(int id)
        {
            return db.Rendimentoes.Count(e => e.RendimentoId == id) > 0;
        }
    }
}